﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * Script de control de los interruptores en el nivel. 
 * Comprueba cuántos de ellos han sido activados
 * y lleva la cuenta de los que quedan por activar.
 */
public class HUDInterruptores: MonoBehaviour {

	List<InterruptorManageActivation> interruptores; //lista de interruptores en el mapa
	List<Toggle> toggles;							 //lista de botones del HUD
	Text activeText;								 //texto de confirmación de que el portal está activo

	int actives = 0;
	bool completed = false;
	bool activated = false;

	// Use this for initialization
	void OnEnable () {
		GetToggles (GetComponentsInChildren<Toggle> ());
		activeText = transform.GetChild(2).GetComponentInChildren<Text> ();
	}

	//Transforma el array de gameobjects de interruptores en una lista de scripts de interruptores
	void GetInterruptores(GameObject[] objects) {
		interruptores = new List<InterruptorManageActivation> ();
		foreach (GameObject obj in objects) {
			interruptores.Add (obj.GetComponent<InterruptorManageActivation> ());
		}
	}

	//Transforma el array de botones del HUD en una lista de componentes
	void GetToggles(Toggle[] objects) {
		toggles = new List<Toggle> ();
		foreach (Toggle obj in objects) {
			toggles.Add (obj.GetComponent<Toggle> ());
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (activated) {
			if (toggles.Count > 0) {
				actives = interruptores.FindAll (x => x.isActive ()).Count;
				interruptores.RemoveAll (x => x.isActive ());
				for (int i = 0; i < actives; i++) {
					toggles [i].isOn = true;
					ColorBlock color = toggles [i].colors;
					color.colorMultiplier = 5;
					toggles [i].colors = color;
				}
				toggles.RemoveAll (x => x.isOn);
			} else {
				if (activeText.enabled == false) {
					activeText.enabled = true;
					completed = true;
				}
			}
		}
	}

	public bool isCompleted() {
		return completed;
	}

	public void activate() {
		activated = true;
		GetInterruptores(GameObject.FindGameObjectsWithTag ("Interruptor"));
	}

	public void deactivate() {
		activated = false;
	}
}
