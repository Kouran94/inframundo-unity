﻿using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class LevelManager : MonoBehaviour {

	public static int current_level = 1;
	public static string filename = "parameters";
	public static List<int[]> level_parameters;

	LoadSceneOnClick levelChanger;

	void Awake() {
		Load ();
		levelChanger = gameObject.GetComponent<LoadSceneOnClick> ();
	}

	private bool Load()
	{
		// Handle any problems that might arise when reading the text
		try
		{
			string line;
			// Create a new StreamReader
			StreamReader theReader = new StreamReader(filename, Encoding.Default);

			using (theReader)
			{
				// While there's lines left in the text file, do this:
				level_parameters = new List<int[]>();
				do
				{
					line = theReader.ReadLine();
					line.TrimStart('[');
					line.TrimEnd(']');
					line.Trim();

					if (line != null)
					{
						int[] list = new int[5];
						string[] entries = line.Split(',');
						list = System.Array.ConvertAll(entries, int.Parse);line.Split(',');
						level_parameters.Add(list);
							
					}
				}
				while (line != null);

				// Done reading, close the reader and return true to broadcast success    
				theReader.Close();
				return true;
			}
		}
		// If anything broke in the try block, we throw an exception with information
		// on what didn't work
		catch (System.Exception e)
		{
			System.Console.WriteLine("{0}\n", e.Message);
			return false;
		}
	}

	public int GetCurrentLevel() {
		return current_level;
	}

	public int[] GetCurrentLevelParameters() {
		return level_parameters [current_level];
	}

	public void NextLevel() {
		current_level++;
		levelChanger.LoadByIndex (current_level);
	}
}
	