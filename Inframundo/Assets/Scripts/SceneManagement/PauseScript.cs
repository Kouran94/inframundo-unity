﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour {

	bool isPause = false;
	public Canvas HUD;
	public Canvas PauseScreen;
	public GameObject audioPanel;
	public GameObject helpPanel;
	public GameObject pausePanel;
	public AudioSource backGroundSound;
	public AudioSource menuSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if( Input.GetButtonDown("Pause")){
			isPause = !isPause;
			// si no esta pausado establecemos timeScale a 0
			if(isPause){
				Time.timeScale = 0;
				backGroundSound.Pause();
				menuSound.Play();
				HUD.enabled = false;
				PauseScreen.enabled = true;
			}else{
				if (!pausePanel.activeSelf) {
					// si estamos en un menu interno salimos al menu de pausa
					audioPanel.SetActive(false);
					helpPanel.SetActive(false);
					pausePanel.SetActive(true);
				} else {
					// si estamos en el menu de pausa continuamos
					PauseScreen.enabled = false;
					HUD.enabled = true;
					backGroundSound.Play();
					menuSound.Stop ();
					Time.timeScale = 1;
				}
			}
		}
	}

	// Button Continue
	public void Continue(){
		// si estamos en el menu de pausa continuamos
		isPause = false;
		Time.timeScale = 1;
		PauseScreen.enabled = false;
		HUD.enabled = true;
		backGroundSound.Play();
		menuSound.Stop ();
	}
}
