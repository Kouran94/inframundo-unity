﻿using UnityEngine;

/*
* Script de llamada a la pantalla de 
* game over cuando el jugador muere.
*/
public class GameOverManager : MonoBehaviour
{
    PlayerHealth playerHealth;       // Reference to the player's health.
    Animator anim;                   // Reference to the animator component.

    void Awake ()
    {
        // Set up the reference.
		playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        anim = GetComponent <Animator> ();
    }


    void Update ()
    {
        // If the player has run out of health...
		if(playerHealth.isDead())
        {
            // ... tell the animator the game is over.
            anim.SetTrigger ("GameOver");
        }
    }
}
