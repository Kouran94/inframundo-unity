﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour {

	public Slider master;
	public Slider effects;
	public Slider music;
	public AudioMixer masterMixer;

	void Start (){
		// establecemos los Sliders y los valores de audio a los valores guardados
		master.value = PlayerPrefs.GetFloat ("masterVol");
		effects.value = PlayerPrefs.GetFloat ("sfxVol");
		music.value = PlayerPrefs.GetFloat ("musicVol");
		masterMixer.SetFloat("masterVol", PlayerPrefs.GetFloat ("masterVol"));
		masterMixer.SetFloat("sfxVol", PlayerPrefs.GetFloat ("sfxVol"));
		masterMixer.SetFloat("musicVol", PlayerPrefs.GetFloat ("musicVol"));
	}

	// si uno de los valores se cambia establecemos el nuevo nivel y guardamos preferencias
	public void SetVolLvl(float volLvl){
		masterMixer.SetFloat("masterVol", volLvl);
		PlayerPrefs.SetFloat("masterVol", volLvl);
	}

	public void SetSfxLvl(float sfxLvl){
		masterMixer.SetFloat("sfxVol", sfxLvl);
		PlayerPrefs.SetFloat("sfxVol", sfxLvl);
	}

	public void SetMusicLvl(float musicLvl){
		masterMixer.SetFloat("musicVol", musicLvl);
		PlayerPrefs.SetFloat("musicVol", musicLvl);
	}

}
