﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateBattle : MonoBehaviour {

	FasesManager manager;

	// Use this for initialization
	void Start () {
		manager = GameObject.Find ("FaseFinalManager").GetComponent<FasesManager>();
	}
	
	// Update is called once per frame
	void OnTriggerStay (Collider col) {
		if (Input.GetButtonDown ("Interactuar") && !manager.isOnBattle()) {
			manager.parseActivation (transform.parent.gameObject);
			Destroy (this.gameObject);
		}
	}
}
