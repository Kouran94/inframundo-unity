﻿using UnityEngine;
using System.Collections;

public class VaseBehaviour : MonoBehaviour
{
	public GameObject DestroyedObject;
	public float timeUnBreakable = 5f;
	bool breakable = false;
	public AudioClip impact;
	AudioSource emisor;

	// Use this for initialization
	void Awake ()
	{
		emisor = GetComponent<AudioSource> ();
		Invoke("isBreakable", timeUnBreakable);
	}

	void OnCollisionEnter (Collision col) {
		if (breakable) {
			emisor.PlayOneShot (impact, col.relativeVelocity.magnitude);
			if (col.relativeVelocity.magnitude > 5f && breakable) {
				DestroyIt ();
			}
		}
	}

	void DestroyIt ()
	{
		if (DestroyedObject) {
			Instantiate (DestroyedObject, transform.position, transform.rotation);
		}
		Destroy (gameObject);
	}

	void isBreakable() {
		breakable = true;
		emisor.enabled = true;
	}
}

