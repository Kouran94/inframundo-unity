﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Script que controla la activación de los interruptores cuando el jugador se aproxima a ellos */
public class InterruptorManageActivation : MonoBehaviour {

	InterruptorAddForce stand;
	bool active = false;

	void Awake() {
		stand = GetComponentsInChildren<InterruptorAddForce> () [0];
		stand.enabled = false;
	}
	
	void OnTriggerEnter(Collider col) {
		if (col.tag == "Player" && !active) {
			stand.enabled = true;
			active = true;
			this.GetComponentInChildren<ParticleSystem> ().Play ();
            this.GetComponent<AudioSource>().Play();
        }
	}

	public bool isActive() {
		return active;
	}
}
