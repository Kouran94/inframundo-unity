﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Script de generación de enemigos y objetos en las habitaciones de forma aleatoria.
 */
public class GeneradorContenido : MonoBehaviour {

	public int maxPropsPerRoom;
	public int maxPlantsPerRoom;
	public int numberOfLevelEnemies;
	public int numberOfLevelVases;
	public int numberOfLevelSwitches;
	public int numberOfLevelHealing;
	public int numberOfLevelTraps;
	public int numberOfLevelCrates;
	public int numberOfRoomCandles;
	public GameObject player;

	public GameObject ENEMY_0;
	public GameObject ENEMY_1;
	public GameObject ENEMY_2;
	public GameObject ENEMY_3;
	public GameObject ENEMY_4;
	public GameObject ENEMY_5;
	public GameObject ENEMY_6;
	public GameObject ENEMY_7;
	public GameObject ENEMY_8;
	public GameObject ENEMY_9;

	public GameObject PROP_0;
	public GameObject PROP_1;
	public GameObject PROP_2;
	public GameObject PROP_3;
	public GameObject PROP_4;
	public GameObject PROP_5;
	public GameObject PROP_6;
	public GameObject PROP_7;
	public GameObject PROP_8;
	public GameObject PROP_9;

	public GameObject PLANT_0;
	public GameObject PLANT_1;
	public GameObject PLANT_2;
	public GameObject PLANT_3;
	public GameObject PLANT_4;
	public GameObject PLANT_5;
	public GameObject PLANT_6;
	public GameObject PLANT_7;
	public GameObject PLANT_8;
	public GameObject PLANT_9;

	public GameObject INTERRUPTOR;
	public GameObject PORTAL;
	public GameObject ESTATUA;
	public GameObject TRAMPA;
	public GameObject VASIJA;
	public GameObject VELA;
	public GameObject CAJA;

	const int MAX_PREFAB = 9;

	const string roomTag = "Rooms";

	List<GameObject> enemylist = new List<GameObject>();
	List<GameObject> proplist = new List<GameObject>();
	List<GameObject> plantlist = new List<GameObject>();

	private GameObject startRoom;

	// Use this for initialization
	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player");
		for (int i = 0; i < MAX_PREFAB; i++) {
			GameObject enemy = (GameObject)this.GetType().GetField("ENEMY_" + i).GetValue(this);
			if (enemy) {
				enemylist.Add (enemy);
			}
			GameObject prop = (GameObject)this.GetType().GetField("PROP_" + i).GetValue(this);
			if (prop) {
				proplist.Add (prop);
			}
			GameObject plant = (GameObject)this.GetType().GetField("PLANT_" + i).GetValue(this);
			if (plant) {
				plantlist.Add (plant);
			}
		}
	}

	public void GenerarContenidos (GameObject startRoom) {
		this.startRoom = startRoom;
		PosicionarJugador ();
		GenerarDecorados ();
		GenerarObjetivos ();
		GenerarEnemigos ();
	}

	void OnEnable() {
//		// posicionamos a los enemigos
//		GenerarDecorados ();
//		PosicionarJugador ();
//		GenerarObjetivos ();
//		GenerarEnemigos ();
	}

	void PosicionarJugador () {
		Vector3 startPos = this.startRoom.transform.position;
		player.transform.position = new Vector3 (startPos.x, player.transform.position.y, startPos.z);

		int ncandles = 12;
		int radius = 12;
		for (int i = 0; i < ncandles; i++) {
			GameObject vela = ClonarEnCentro (startRoom, VELA);
			vela.transform.Translate (radius * (Quaternion.Euler(0f, i*360f/ncandles, 0f) * Vector3.forward));
		}
	}

	void GenerarEnemigos () {
		if (enemylist.Count != 0) {		
			GameObject[] rooms = GameObject.FindGameObjectsWithTag (roomTag);
			for (int i=0; i<numberOfLevelEnemies; i++) {
				int roomindex = Random.Range (0, rooms.Length);
				int enemyindex = (int)(Random.value * enemylist.Count);
				ClonarAleatorio (rooms [roomindex], enemylist [enemyindex]);
			}
		}
	}

	void GenerarDecorados () {
		GameObject[] rooms = GameObject.FindGameObjectsWithTag(roomTag);
		foreach (GameObject room in rooms) {
			if (proplist.Count != 0) {
				int props = (int)(Random.value * maxPropsPerRoom);

				for (int i = 0; i < props; i++) {
					int propindex = (int)(Random.value * proplist.Count);
					ClonarAleatorio (room, proplist [propindex]);
				}
			}

			if (plantlist.Count != 0) {
				int plants = (int)(Random.value * maxPlantsPerRoom);

				for (int i = 0; i < plants; i++) {
					int plantindex = (int)(Random.value * plantlist.Count);
					ClonarAleatorio (room, plantlist [plantindex]);
				}
			}
		}
	}

	GameObject ClonarAleatorio (GameObject room, GameObject prefab) {
		Vector3 rndPosWithin;
		rndPosWithin = new Vector3 (Random.Range (-0.9f, 0.9f), 0, Random.Range (-0.9f, 0.9f));
		rndPosWithin = room.transform.TransformPoint (rndPosWithin * .5f) + Vector3.up * prefab.transform.position.y;
		GameObject go = (GameObject)Instantiate (prefab, rndPosWithin, prefab.transform.rotation);
		go.transform.parent = this.transform;
		return go;
	}

	GameObject ClonarEnCentro(GameObject room, GameObject prefab) {
		// lo colocamos en el centro de la habitación
		GameObject go = (GameObject)Instantiate (prefab, room.transform.position + new Vector3 (0f, prefab.transform.position.y, 0f), prefab.transform.rotation);
		go.transform.parent = this.transform;

		return go;
	}

	void GenerarObjetivos () {
		int roomi;
		GameObject room;

		// COSAS MUTUAMENTE EXCLUSIVAS EN LAS HABITACIONES

		// lista de habitaciones que vamos borrando
		List<GameObject> roomList = new List<GameObject>();
		foreach (GameObject g in GameObject.FindGameObjectsWithTag(roomTag)) {
			roomList.Add (g);
		}

		// posicionamos los interruptores
		if (INTERRUPTOR) {
			for (int i = 0; i < numberOfLevelSwitches; i++) {
				roomi = (int)Random.Range (0, roomList.Count - 1);
				room = roomList [roomi];
				roomList.RemoveAt (roomi);
				ClonarEnCentro (room, INTERRUPTOR);
				if (VELA) {
					for (int j = 0; j < numberOfRoomCandles; j++) {
						// lo colocamos aleatoriamente por la habitación
						ClonarAleatorio (room, VELA);
					}
				}
			}
		}

		// el portal
		if (PORTAL) {
			roomi = (int)Random.Range (0, roomList.Count - 1);
			room = roomList [roomi];
			roomList.RemoveAt (roomi);
			ClonarEnCentro (room, PORTAL);
			if (VELA) {
				for (int j = 0; j < numberOfRoomCandles; j++) {
					// lo colocamos aleatoriamente por la habitación
					ClonarAleatorio (room, VELA);
				}
			}
		}

		// y las estatuas de curación
		if (ESTATUA) {
			for (int i = 0; i < numberOfLevelHealing; i++) {
				roomi = (int)Random.Range (0, roomList.Count - 1);
				room = roomList [roomi];
				roomList.RemoveAt (roomi);
				ClonarEnCentro (room, ESTATUA);
				if (VELA) {
					for (int j = 0; j < numberOfRoomCandles; j++) {
						// lo colocamos aleatoriamente por la habitación
						ClonarAleatorio (room, VELA);
					}
				}
			}
		}

		// COSAS QUE SE PUEDEN COLOCAR VARIAS VECES EN LA MISMA HABITACIÓN

		// lista completa de habitaciones
		List<GameObject> roomListComplete;

		if (TRAMPA) {
			roomListComplete = new List<GameObject>();
			foreach (GameObject g in GameObject.FindGameObjectsWithTag(roomTag)) {
				roomListComplete.Add (g);
			}
			for (int i = 0; i < numberOfLevelTraps; i++) {
				roomi = (int)Random.Range (0, roomListComplete.Count - 1);
				room = roomListComplete [roomi];
				// lo colocamos aleatoriamente por la habitación
				ClonarAleatorio (room, TRAMPA);
			}
		}

		if (VASIJA) {
			roomListComplete = new List<GameObject>();
			foreach (GameObject g in GameObject.FindGameObjectsWithTag(roomTag)) {
				roomListComplete.Add (g);
			}
			for (int i = 0; i < numberOfLevelVases; i++) {
				roomi = (int)Random.Range (0, roomListComplete.Count - 1);
				room = roomListComplete [roomi];
				// lo colocamos aleatoriamente por la habitación
				ClonarAleatorio (room, VASIJA);
			}
		}

		if (CAJA) {
			roomListComplete = new List<GameObject>();
			foreach (GameObject g in GameObject.FindGameObjectsWithTag(roomTag)) {
				roomListComplete.Add (g);
			}
			for (int i = 0; i < numberOfLevelCrates; i++) {
				roomi = (int)Random.Range (0, roomListComplete.Count - 1);
				room = roomListComplete [roomi];
				// lo colocamos aleatoriamente por la habitación
				ClonarAleatorio (room, CAJA);
			}
		}
	}

}
