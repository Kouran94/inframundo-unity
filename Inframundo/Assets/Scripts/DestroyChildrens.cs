﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyChildrens : MonoBehaviour {

    public float time;

	// Use this for initialization
	void Start () {
        //this.transform.GetComponentsInChildren<GameObject>();
        foreach (Transform child in transform)
        {
            child.gameObject.AddComponent<DestroyAfterTime>();
            child.gameObject.GetComponent<DestroyAfterTime>().time = time;
        }
    }

	
	// Update is called once per frame
	void Update () {
		
	}
}
