﻿using System.Collections;
using UnityEngine;

/*
 * Script de la cámara principal para seguir al jugador y rotar con él.
 */
public class CameraFollow : MonoBehaviour
{
	Transform target;
	float smoothing = 1f;        // The speed with which the camera will be following.+
	Vector3 offset;              // The initial offset from the target.

	void Start ()
	{
		target = gameObject.GetComponentInParent<Transform>();
		offset = transform.position - target.position;
	}

	void FixedUpdate ()
	{
		// Create a postion the camera is aiming for based on the offset from the target.
		Vector3 targetCamPos = target.position + offset;

		// Smoothly interpolate between the camera's current position and it's target position.
		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);

	}
}