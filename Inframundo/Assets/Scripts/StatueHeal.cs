﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/*
* Script de la estatua de curación de un único uso.
*/
public class StatueHeal : MonoBehaviour {

	public static int healing_amount = 50;

	Light luz;
	ParticleSystem particulas;
	PlayerHealth player;

	bool used = false;

	// Use this for initialization
	void Start () {
		luz = GetComponent<Light> ();
		particulas = GetComponent<ParticleSystem> ();
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHealth> ();
	}

	void OnTriggerStay(Collider col) {
		if (!used && col.transform.tag == "Player") {
			if (Input.GetButtonDown ("Interactuar")) {
				used = true;
				player.Heal (healing_amount);
				luz.enabled = false;
				particulas.Stop ();
			}
		}
	}
		
}
