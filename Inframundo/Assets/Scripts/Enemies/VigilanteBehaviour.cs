﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/*
* Comportamiento de los enemigos vigilantes (esqueletos)
*/
public class VigilanteBehaviour : MonoBehaviour
{

	public float SUSPICIOUS_DELAY;
	//cuánto tiempo tarda en verte
	public float UNALERTED_DELAY;
	//tiempo que tarda en dejar de perseguirte
	public float visual_distance;
	//distancia máxima a la que te ve
	public int visual_angle;
	//ángulo de visión positivo y negativo desde transform.forward

	VigilanteAttack attack;
	//Script para el comportamiento de ataque
	Transform player;
	//Posición del jugador.
	Distractible distracciones;
	PlayerDetector detector;
	PlayerMovement playerMovement;
	//Script de movimiento del jugador.
	PlayerHealth playerHealth;
	//Script de salud del jugador.
	Animator anim;
	//Gestor de animaciones del enemigo.
	NavMeshAgent nav;
	Vector3 destiny;

	bool alerted = false, searching = false, active = true;
	float timerUP = 0f, timerDOWN = 0f;

	public AudioClip[] pasos;
	AudioSource audioSourc;
	public float timeWaitRun = 0.3F;

	void Awake ()
	{
		nav = GetComponent<NavMeshAgent> ();
		nav.speed = 5f;
		distracciones = gameObject.GetComponent<Distractible> ();
		detector = gameObject.GetComponent<PlayerDetector> ();
		anim = gameObject.GetComponent<Animator> ();
		attack = gameObject.GetComponent<VigilanteAttack> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		playerMovement = player.GetComponent<PlayerMovement> ();
		playerHealth = player.GetComponent <PlayerHealth> ();
		Idle ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (playerHealth.isDead ()) {
			Idle ();
		} else {
			CheckVisualField ();
			if (!alerted)
				CheckSounds ();
		}
	}

	//El enemigo no patrulla ni ataca, está quieto en el sitio.
	void Idle ()
	{
		DeactivateSearch ();
		DeactivateAttack ();
	}

	//El enemigo ataca al jugador cuando lo detecta.
	void Attack ()
	{
		DeactivateSearch ();
		nav.ResetPath ();
		nav.speed = 10f;
		nav.isStopped = false;
		alerted = true;
		anim.SetBool ("Alerted", alerted);
		attack.enabled = true;
		detector.AlarmCooldown (UNALERTED_DELAY - timerDOWN); // informamos al sistema de detección del tiempo que falta para que termine la alarma
	}

	void DeactivateAttack ()
	{
		attack.enabled = false;
		nav.ResetPath ();
		anim.SetBool ("Alerted", false);
		alerted = false;
	}

	void Search ()
	{
		nav.speed = 8f;
		anim.SetBool ("Searching", true);
		nav.SetDestination (destiny);
		nav.isStopped = false;
	}

	void DeactivateSearch ()
	{
		nav.ResetPath ();
		anim.SetBool ("Searching", false);
		searching = false;
	}

	//Se comprueba si el enemigo debe activarse y si el jugador está dentro de su campo de visión.
	void CheckVisualField ()
	{
		if (active) {
			Vector3 direction = player.position - transform.position;
			RaycastHit _hit;
			bool seeing = false;
			if (Physics.Raycast (transform.position, direction, out _hit, visual_distance) && _hit.transform.gameObject.tag == "Player") {
				float angle = Vector3.Angle (transform.forward, direction);
				seeing = (angle >= -visual_angle && angle <= visual_angle);
			}
			if (seeing) { // Si estamos viendo al jugador contamos el tiempo que está en rango de visión
				transform.LookAt (player.position);
				timerDOWN = 0f;
				if (!alerted) {
					detector.Suspect (SUSPICIOUS_DELAY - timerUP); // actualizamos el estado del sistema de detección
					timerUP += Time.deltaTime;
					if (timerUP >= SUSPICIOUS_DELAY) { //si se supera un umbral de tiempo, se pone alerta
						Attack ();
					}
				}
			} else { //Si no estamos viendo al jugador, contamos el tiempo que no está en rango de visión
				timerUP = 0f;
				if (alerted) {
					timerDOWN += Time.deltaTime;
					detector.AlarmCooldown (UNALERTED_DELAY - timerDOWN); // informamos al sistema de detección del tiempo que falta para que termine la alarma
					if (timerDOWN >= UNALERTED_DELAY) {	//si supera un umbral de tiempo, deja de estar alerta
						DeactivateAttack ();
						detector.AlarmEnd (); // informamos de que la alarma ha terminado
					}
				}
			}
		}
	}

	void CheckSounds ()
	{
		if (searching) {
			if (Vector3.Distance (transform.position, nav.destination) <= 5f) {
				DeactivateSearch ();
			}
		} else {
			distracciones.CheckDistracted (out searching, out destiny);
			if (searching) {
				Search ();
			}
		}

	}

	void OnCollisionEnter (Collision col)
	{
		//Si el jugador choca contra el enemigo, él se da cuenta.
		if (nav != null) {
			if (col.gameObject.tag == "Player") {
				Attack ();
				timerDOWN = 0f;
			} else if (col.gameObject.tag == "Throwable" && searching) {
				nav.isStopped = true;
				DeactivateSearch ();
				anim.SetTrigger ("Hit");
			}
		}
	}

}
