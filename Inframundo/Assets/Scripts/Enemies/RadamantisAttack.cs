﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadamantisAttack : MonoBehaviour {

	ContarTiempo contador;
	FasesManager manager;

	// Use this for initialization
	void OnEnable () {
		contador = GameObject.Find ("Contador").GetComponent<ContarTiempo> ();
		manager = GameObject.Find ("FaseFinalManager").GetComponent<FasesManager> ();
		contador.StartCounter ();
	}
	
	// Update is called once per frame
	void Update () {
		if (contador.isFinished ()) {
			manager.parseDeactivation (transform.parent.gameObject);
		}
	}
}
