﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

/*
* Script de comportamiento de ataque de los enemigos vigilantes (esqueletos).
*/
public class VigilanteAttack : MonoBehaviour
{
	public float timeBetweenAttacks = 2f;     // The time in seconds between each attack.
	public int attackDamage = 10;               // The amount of health taken away per attack.
	public float attack_range = 2.5f;
	public float velocidad = 10f;

	NavMeshAgent nav;
	GameObject player;                          // Reference to the player GameObject.
	PlayerHealth playerHealth;                  // Reference to the player's health.
	float timer;                                // Timer for counting up to the next attack.
	Vector3 last_seen;

	public AudioClip atack;
	public AudioClip[] paso;
	AudioSource audioSourc;
	public float timeWaitRun = 0.3F;
	Vector3 lastPosition;
	float countTime=0;

	//Cuando el script de comportamiento del enemigo activa el script de ataque...
	void OnEnable ()
	{ 
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <PlayerHealth> ();
		nav = GetComponent<NavMeshAgent> ();
		audioSourc = GetComponent<AudioSource> ();
		nav.speed = velocidad;
		nav.ResetPath ();
		last_seen = player.transform.position;
	}
		
	void Update ()
	{
		timer += Time.deltaTime;
		countTime += Time.deltaTime;
		Vector3 direction = player.transform.position - transform.position;
		RaycastHit _hit;
		if (Physics.Raycast (transform.position, direction, out _hit, 20f)) {
			if (_hit.transform.gameObject.tag == "Player") {
				last_seen = player.transform.position;
			}
		}

		transform.LookAt (last_seen);
		float distance = Vector3.Distance (last_seen, transform.position);

		if (distance <= this.attack_range) {
			nav.isStopped = true;
			if (timer >= timeBetweenAttacks)
				Attack ();
			nav.isStopped = false;
		} else //si no, se mueve hacia él.
			nav.SetDestination (last_seen);
		
		if (countTime > timeWaitRun && isMoving()){
			audioSourc.PlayOneShot (paso[(Mathf.RoundToInt(Random.value))%paso.Length],0.5F);
			countTime = 0;
		}
		lastPosition = transform.position;
	}

	void Attack ()
	{
		timer = 0f;
		if(!playerHealth.isDead()) //Si el jugador está vivo, le quitamos vida.
			playerHealth.TakeDamage (attackDamage);
			audioSourc.PlayOneShot (atack, 0.9F);
	}

	public bool isMoving(){
		return 0.2 < Vector3.Distance(transform.position, lastPosition);
	}
}