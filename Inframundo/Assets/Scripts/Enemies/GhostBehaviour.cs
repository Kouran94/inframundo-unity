﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBehaviour : MonoBehaviour {

	public float appearTime = 5f;
	public float accelerationRate = 2f;
	public float initialSpeed = 1f;

	float speed = 0f;
	float timer = 0f;

	bool appeared = false;
	bool moving = false;

	Transform player;
	PlayerMovement playerMovement;
	PlayerHealth playerHealth;
	ParticleSystem smoke;
	GameObject monster; 

	// Use this for initialization
	void Start () {
		monster = transform.GetChild (0).gameObject;
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		playerMovement = player.GetComponent<PlayerMovement> ();
		playerHealth = player.GetComponent<PlayerHealth> ();
		smoke = GetComponent<ParticleSystem> ();
		speed = initialSpeed;
		monster.SetActive(false);
		smoke.Stop ();

	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (playerMovement.isHidden ()) {
			if (!appeared) {
				if (timer >= appearTime) {
					appeared = true;
					monster.SetActive(true);
					smoke.Play ();
					timer = 0f;
				}
			} else {
				if (timer >= appearTime) {
					timer = 0f;
					if (moving) {
						speed *= accelerationRate;
					} else {
						moving = true;
					}
				}
			}
		} else {
			moving = false;
			speed = initialSpeed;
		}
		if (moving) {
			Vector3 direction = player.transform.position - transform.position;
			direction.Normalize ();
			transform.LookAt (player.transform);
			transform.Translate (direction * speed * Time.deltaTime);
		}
	}

	void OnCollisionEnter(Collision col) {
		if (col.transform.tag == "Player") {
			playerHealth.TakeDamage (50);
		} else {
			accelerationRate += 0.5f;
			Destroy (col.gameObject);
		}
	}
}
