﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/*
 * Script de comportamiento del enemigo estatua (golem)
 */
public class GolemBehaviour : MonoBehaviour {

	GolemAttack attack;
	Animator anim;
	Light luz;
	Transform player;
	NavMeshAgent nav;
	PlayerDetector detect;

	public float follow_range = 50f;	//Rango de persecución antes de quedarse parado de nuevo
	bool active = false;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		attack = gameObject.GetComponent<GolemAttack> ();
		nav = gameObject.GetComponent<NavMeshAgent> ();
		anim = GetComponent <Animator> ();
		luz = GetComponentInChildren<Light> ();
		detect = GetComponent<PlayerDetector> ();
		luz.enabled = false;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		//Si el enemigo está activo, comprobamos a cuánta distancia del jugador se encuentra por si hay que desactivarlo.
		if (active) {
			float distance = Vector3.Distance (transform.position, player.transform.position);
			if (distance >= follow_range) {
				Disable ();
			}
		}
	}

	void OnTriggerEnter(Collider col) 
	{
		//Si la luz del jugador ilumina al enemigo, este se activa.
		if (col.transform.tag == "Lights" && !active)
			Enable ();
	}

	//Activa al enemigo, su luz, el script de ataque y la animación de movimiento.
	void Enable() {
		active = true;
		nav.enabled = true;
		luz.enabled = true;
		attack.enabled = true;
		anim.SetBool ("Iluminated", true);
		detect.AlarmCooldown (0f);
	}

	//Desactiva al enemigo, su luz, el script de ataque y la animación de movimiento.
	void Disable() {
		active = false;
		nav.enabled = false;
		luz.enabled = false;
		attack.enabled = false;
		anim.SetBool ("Iluminated", false);
		detect.AlarmEnd ();
	}

//	void OnCollisionEnter(Collision col) {
//		if (col.gameObject.tag == "Throwable") {
//			nav.isStopped = true;
//			anim.SetTrigger ("Hit");
//		}
//	}

}
