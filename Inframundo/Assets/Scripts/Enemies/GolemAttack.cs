﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

/*
 * Script de ataque del enemigo estatua (golem)
 */
public class GolemAttack : MonoBehaviour
{
	public float timeBetweenAttacks = 3f;     // The time in seconds between each attack.
	public int attackDamage = 20;             // The amount of health taken away per attack.
	public float attack_range = 5f;			  // Rango mínimo de distancia para que el enemigo empiece a atacar.
	public float velocidad = 4f;

	NavMeshAgent nav;
	Animator anim;
	GameObject player;                          // Reference to the player GameObject.
	PlayerHealth playerHealth;                  // Reference to the player's health.
	float timer;                                // Timer for counting up to the next attack.

	public AudioClip atack;
	public AudioClip[] paso;
	AudioSource audioSourc;
	public float timeWaitRun = 0.8F;
	Vector3 lastPosition;
	float countTime=0;

	void OnEnable ()
	{ 
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <PlayerHealth> ();
		anim = GetComponent<Animator> ();
		nav = GetComponent<NavMeshAgent> ();
		audioSourc = GetComponent<AudioSource> ();
		nav.speed = velocidad;
		nav.ResetPath ();
	}

	void Update ()
	{
		timer += Time.deltaTime;
		countTime += Time.deltaTime;
		transform.LookAt (player.transform);
		float distance = Vector3.Distance (player.transform.position, transform.position);

		if (distance <= attack_range) {
			nav.isStopped = true;
			if (timer >= timeBetweenAttacks) {
				anim.SetTrigger ("Attack"); 
				Attack ();
			}
			nav.isStopped = false;
		} else
			nav.SetDestination (player.transform.position);
		if (countTime > timeWaitRun && isMoving()){
			Debug.Log("sonido de golem corriendo lanzado");
			audioSourc.PlayOneShot (paso[(Mathf.RoundToInt(Random.value))%paso.Length],0.5F);
			countTime = 0;
		}
		lastPosition = transform.position;
	}

	void Attack ()
	{
		// Reset the timer.
		timer = 0f;

		if (!playerHealth.isDead())
			playerHealth.TakeDamage (attackDamage);
		audioSourc.PlayOneShot (atack, 0.9F);
	}

	public bool isMoving(){
		return 0.05 < Vector3.Distance(transform.position, lastPosition);
	}
}