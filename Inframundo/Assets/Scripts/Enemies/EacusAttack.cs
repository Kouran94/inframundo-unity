﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EacusAttack : MonoBehaviour {

	public GameObject bullet;
	GameObject player;
	Animator anim;
	float speed = 500f;
	float timer = 0f;
	float attackFreq = 1.5f;
	AudioSource disparoSound;
	FasesManager manager;
	List<Torchelight> antorchas;

	Vector3 firePosition;

	bool dead = false;

	void OnEnable() {
		//Disable barrier so that bullets can reach player
		anim = GetComponent<Animator>();
		manager = GameObject.Find("FaseFinalManager").GetComponent<FasesManager>();
		player = GameObject.FindGameObjectWithTag ("Player");
		firePosition = this.transform.position;
		firePosition.y += 2f;
		disparoSound = GetComponent<AudioSource> ();
		GetAntorchas(GameObject.FindGameObjectsWithTag ("Torch"));
	}

	void GetAntorchas(GameObject[] objects) {
		antorchas = new List<Torchelight> ();
		foreach (GameObject obj in objects) {
			antorchas.Add (obj.GetComponent<Torchelight> ());
		}
	}

	void Update () 
	{
		timer += Time.deltaTime;
		this.transform.LookAt (player.transform);
		// Put this in your update function
		if (!dead) {
			if (timer >= attackFreq) {
				anim.ResetTrigger ("Attack");
				anim.SetTrigger ("Attack");
				disparoSound.PlayOneShot (disparoSound.clip);
				timer = 0f;
				// Instantiate the projectile at the position and rotation of this transform
				GameObject clone = Instantiate (bullet, firePosition, this.transform.rotation);
				clone.GetComponent<Rigidbody> ().AddForce (this.transform.forward * speed);
			}
			antorchas.RemoveAll (x => x.On);
			if (antorchas.Count == 0) {
				//Si todas las antorchas han sido encendidas, acaba la batalla
				anim.SetTrigger ("Die");
				dead = true;
				timer = 0f;
			}
		} else {
			//Esperamos hasta que la animación de muerte termine para desactivar 
			if (timer >= 1f)
				manager.parseDeactivation (transform.parent.gameObject);
		}
	}

}
