﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinosAttack : MonoBehaviour {

	FasesManager manager;
	HUDInterruptores interruptores;
	public GameObject bullet;
	Transform player;
	ParticleSystem fuego;

	public int damage = 10;

	float retardoAtaque = 2f;
	float retardo = 0.5f;
	float timer = 0f;
	float attack_timer = 0f;
	bool finished = false;
	Quaternion rot;

	// Use this for initialization
	void OnEnable () {
		manager = GameObject.Find("FaseFinalManager").GetComponent<FasesManager>();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		rot = Quaternion.LookRotation (transform.position - player.position, Vector3.forward);
		rot.x = 0;
		rot.z = 0;
		fuego = GetComponentInChildren<ParticleSystem> ();
		fuego.Play ();
		gameObject.GetComponentInChildren<FlameCone> ().enabled = true;
		interruptores = GameObject.Find ("HUDFinal").GetComponentInChildren<HUDInterruptores> ();
		interruptores.activate ();
	}

	// Update is called once per frame
	void Update () {
		finished = interruptores.isCompleted ();
		if (!finished) {
			fuego.Play ();
			timer += Time.deltaTime;
			attack_timer += Time.deltaTime;

			if (timer >= retardo) {
				timer = 0f;
				rot = Quaternion.LookRotation (transform.position - player.position, Vector3.forward);
				rot.x = 0;
				rot.z = 0;
				fuego.Stop ();
			}

			transform.rotation = Quaternion.Slerp (transform.rotation, rot, Time.deltaTime * 5);
		} else {
			fuego.Stop ();
			manager.parseDeactivation (transform.parent.gameObject);
			interruptores.deactivate ();
		}
	}

	public void OnTriggerStayChild (Collider col) {
		if (enabled) {
			Vector3 direction = col.transform.position - fuego.transform.position;
			RaycastHit _hit;
			if (Physics.Raycast (fuego.transform.position, direction, out _hit, 100f)) {
				if (_hit.transform.gameObject.tag == "Player") { 
					if (attack_timer > retardoAtaque) {
						attack_timer = 0f;
						player.gameObject.GetComponent<PlayerHealth> ().TakeDamage (damage);
					}
				}
			}
		}
	}
}
