﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEnemy : MonoBehaviour {

	public AudioClip[] audioArray;
	Vector3 oldVector;
		

	// Use this for initialization
	void Start () {
		oldVector = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		float curDistance = Vector3.Distance(transform.position, oldVector);
		if (curDistance > 1)
			this.GetComponent<AudioSource> ().PlayOneShot (audioArray [0]);
	}


}
