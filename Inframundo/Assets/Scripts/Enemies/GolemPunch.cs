﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Script para el collider del puño enemigo estatua
 */
public class GolemPunch : MonoBehaviour {

	bool hit = false;

	void OnTriggerEnter(Collider col) 
	{
		if (col.transform.tag == "Player")
			hit = true;
	}

	public bool isHit() {
		return hit;
	}

	public void unHit() {
		hit = false;
	}
}
