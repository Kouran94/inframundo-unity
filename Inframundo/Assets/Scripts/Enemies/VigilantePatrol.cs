﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

/*
* Comportamiento de patrulla aleatoria de los enemigos vigilantes (esqueletos)
*/
public class VigilantePatrol : MonoBehaviour {

	public float speed = 5f;	//Velocidad de patrulla
	public int range = 10;		//Distancia máxima del punto de destino a calcular en el NavMesh

	NavMeshAgent nav;
	Vector3 destiny;
	NavMeshHit hit;

	void OnEnable() {
		nav = GetComponent<NavMeshAgent> ();
		nav.speed = speed;
		nav.ResetPath ();
		RecalculateDestiny ();
		SetDestination ();
	}

	void OnDisable() {
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance(transform.position, nav.destination) <= 1f) {
			nav.isStopped = true;
			RecalculateDestiny ();
			SetDestination ();
			nav.isStopped = false;
		}
	}

	void RecalculateDestiny() {
		destiny = Random.insideUnitSphere * range;
		destiny += transform.position;
	}

	void SetDestination() {
		NavMesh.SamplePosition (destiny, out hit, range, 1);
		nav.SetDestination (hit.position);
	}

}
