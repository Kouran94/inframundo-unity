﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Distractible : MonoBehaviour {

	// er radio ar que escuxa lah distrasione la criatura
	public float hearingRadius;

	private bool distracted = false;
	private Vector3 distraction;

	// queue stuff
	private const int QUEUE_SIZE = 5;
	private static int headIndex = 0;
	private static int tailIndex = 0;

	// Nodos de la cola de distracciones (structs para quitar overhead referencial)
	private struct QueueNode {
		public Vector3 where;
		public float ttl;
		public float startTime;
	}

	private static int size {
		get {
			int dist = tailIndex - headIndex;
			if (dist < 0) {
				return QUEUE_SIZE + (dist);
			} else {
				return QUEUE_SIZE - (dist);
			}
		}
	}

	private static QueueNode[] distractions = new QueueNode[QUEUE_SIZE];

	private bool CheckDistractions() {
		int index = 0;
		while(index < size) {
			QueueNode dist = distractions [(index + headIndex) % QUEUE_SIZE];
			if (Time.deltaTime - dist.startTime > dist.ttl)
				RemoveDistraction ();
			else {
				if ((dist.where - this.transform.position).magnitude < hearingRadius) {
					GoCheckOut (dist.where);
					return true;
				}
			}
			index++;
		}
		return false;
	}

	/**
	 * Este método implementa el comportamiento de ir al sitio indicado por Vector3 a comprobar el ruido.
	 */
	private void GoCheckOut (Vector3 somewhere) {
		distracted = true;
		distraction = somewhere;
		//print ("TODO: " + this.name + " ha escuchado algo en " + somewhere + ". Vamos a ver que hay... solo que no porque nadie ha escrito el código para ir hasta allá pero eso sería lo que pasaría ahora.");
		// habría que reactivarlo al terminar el proceso de checkeado desde el script que lo maneje
	}

	private static void RemoveDistraction() {
		if (size > 0) {
			headIndex++;
		}
	}

	public static bool AddDistraction(Vector3 where, float ttl) {
		if (size >= QUEUE_SIZE) {
			return false;
		}

		distractions [tailIndex].where = where;
		distractions [tailIndex].ttl = ttl;
		distractions [tailIndex].startTime = Time.deltaTime;
		tailIndex++;
		return true;
	}
	
	void Update () {
		// cuando se detecta distracción, detener comportamiento
		if (CheckDistractions ()) {
			enabled = false;
		}
	}

	public void CheckDistracted(out bool search, out Vector3 position) {
		//devuelve la posición a comprobar si hay alguna y la elimina
		search = this.distracted;
		position = this.distraction;
		distracted = false;
	}
}
