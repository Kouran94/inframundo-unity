﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour
{
    public GameObject effect;
    public float time;
    // Use this for initialization
    void Start()
    {
        Invoke("Explode", time + Random.value*10);
    }
    void Explode()
    {
        //Instantiate(effect, gameObject.transform.position, gameObject.transform.rotation);
        Destroy(gameObject);
    }
}