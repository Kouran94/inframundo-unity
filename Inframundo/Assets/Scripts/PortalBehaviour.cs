﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Script de comportamiento del portal. 
 * Incluye su activación, las partítulas y animaciones y el traslado al siguiente nivel.
 */
public class PortalBehaviour : MonoBehaviour {

	Light luz;
	ParticleSystem portal;
	HUDInterruptores hud;

	AudioSource audioSourc;


	bool firsttime = true;

	// Use this for initialization
	void Start () {
		hud = GameObject.FindGameObjectWithTag ("InterruptorHUD").GetComponent<HUDInterruptores> ();
		hud.activate ();
		luz = gameObject.GetComponentInChildren<Light> ();
		portal = gameObject.GetComponentInChildren<ParticleSystem> ();
		luz.enabled = false;
		portal.Stop ();
		audioSourc = GetComponent<AudioSource> ();
	}
		
	// Update is called once per frame
	void Update () {
		if (hud.isCompleted() && firsttime) {
			firsttime = false;
			luz.enabled = true;
			portal.Play ();
			audioSourc.Play ();
		}
	}

	//Cuando el jugador está dentro del collider del portal, si pulsa el botón de interactuar 
	// se comprueba si se cumplen los requisitos para pasar de nivel.
	void OnTriggerStay(Collider col) {
		if (col.transform.tag == "Player" && Input.GetButtonDown("Interactuar")) {
			if (hud.isCompleted()) {
				print ("Pasas al siguiente nivel");
				int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
				SceneManager.LoadScene(nextSceneIndex);
			}
			else
				print ("Quedan portales por activar.");
		}
			
	}
}
