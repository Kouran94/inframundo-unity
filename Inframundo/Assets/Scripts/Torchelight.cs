using UnityEngine;
using System.Collections;

public class Torchelight : MonoBehaviour {
	
	public GameObject TorchLight;
	public GameObject MainFlame;
	public GameObject BaseFlame;
	public GameObject Etincelles;
	public GameObject Fumee;
	public float MaxLightIntensity;
	public float IntensityLight;
    public bool On;

    void Start () {
		TorchLight.GetComponent<Light>().intensity=IntensityLight;
        ParticleSystem.EmissionModule Mf = MainFlame.GetComponent<ParticleSystem>().emission;
        Mf.rateOverTime=IntensityLight*20f;
        ParticleSystem.EmissionModule Bf = BaseFlame.GetComponent<ParticleSystem>().emission;
        Bf.rateOverTime =IntensityLight*15f;
        ParticleSystem.EmissionModule Et = Etincelles.GetComponent<ParticleSystem>().emission;
        Et.rateOverTime =IntensityLight*7f;
        ParticleSystem.EmissionModule Fm = Fumee.GetComponent<ParticleSystem>().emission;
        Fm.rateOverTime =IntensityLight*12f;
	}


    void Update()
    {
        if (IntensityLight < 0) IntensityLight = 0;
        if (IntensityLight > MaxLightIntensity) IntensityLight = MaxLightIntensity;
        if (On)
        {
            TorchLight.GetComponent<Light>().intensity = IntensityLight / 2f + Mathf.Lerp(IntensityLight - 0.1f, IntensityLight + 0.1f, Mathf.Cos(Time.time * 30));

            //TorchLight.GetComponent<Light>().color=new Color(Mathf.Min(IntensityLight/1.5f,1f),Mathf.Min(IntensityLight/2f,1f),0f);
            //TorchLight.GetComponent<Light>().color = new Color(Mathf.Min(IntensityLight / 1.5f, 1f), Mathf.Min(IntensityLight / 2f, 1f), 0f);

            ParticleSystem.EmissionModule Mf = MainFlame.GetComponent<ParticleSystem>().emission;
            Mf.rateOverTime = IntensityLight * 20f;
            ParticleSystem.EmissionModule Bf = BaseFlame.GetComponent<ParticleSystem>().emission;
            Bf.rateOverTime = IntensityLight * 15f;
            ParticleSystem.EmissionModule Et = Etincelles.GetComponent<ParticleSystem>().emission;
            Et.rateOverTime = IntensityLight * 7f;
            ParticleSystem.EmissionModule Fm = Fumee.GetComponent<ParticleSystem>().emission;
            Fm.rateOverTime = IntensityLight * 12f;
        }
        else
        {
            TorchLight.GetComponent<Light>().intensity = 0;
            ParticleSystem.EmissionModule Mf = MainFlame.GetComponent<ParticleSystem>().emission;
            Mf.rateOverTime = 0;
            ParticleSystem.EmissionModule Bf = BaseFlame.GetComponent<ParticleSystem>().emission;
            Bf.rateOverTime = 0;
            ParticleSystem.EmissionModule Et = Etincelles.GetComponent<ParticleSystem>().emission;
            Et.rateOverTime = 0;
            ParticleSystem.EmissionModule Fm = Fumee.GetComponent<ParticleSystem>().emission;
            Fm.rateOverTime = 0;
        }
    }
}
