﻿using UnityEngine;
using System.Collections;

/*
 * Script de la cámara del mapa, que sigue al jugador desde arriba 
 * y proporciona una visión de pájaro del mapa.
 */
public class CameraMap : MonoBehaviour
{

	Transform target;
	float smoothing = 5f;        // The speed with which the camera will be following.+
	Vector3 offset;              // The initial offset from the target.

	void Start ()
	{
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		//Situamos la cámara del mapa en su lugar, justo donde el jugador, pero con un valor más alto en el eje Y
		transform.position = target.transform.position + new Vector3 (0f, 20f, 0f);
		offset = transform.position - target.position;
	}

	void FixedUpdate ()
	{
		// Create a postion the camera is aiming for based on the offset from the target.
		Vector3 targetCamPos = target.position + offset;

		// Smoothly interpolate between the camera's current position and it's target position.
		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);
	}
}

