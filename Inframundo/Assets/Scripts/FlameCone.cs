﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameCone : MonoBehaviour {

	MinosAttack attack;

	// Use this for initialization
	void OnEnable () {
		attack = transform.parent.gameObject.GetComponent<MinosAttack> ();
	}

	void OnTriggerStay(Collider col) {
		if (enabled && col.transform.tag == "Player")
			attack.OnTriggerStayChild (col);
	}

}
