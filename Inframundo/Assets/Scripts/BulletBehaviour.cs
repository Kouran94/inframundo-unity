﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour {

	PlayerHealth playerHealth;
	public int damage = 10;

	void Start() {
		playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
	}

	void OnCollisionEnter(Collision col)
	{
		switch (col.transform.tag) 
		{
		case "Player":
			playerHealth.TakeDamage (damage);
			break;
		case "Torch":
			col.gameObject.GetComponentInParent<Torchelight> ().On = true;
			break;
		}
		Destroy (this.gameObject, 0f);
	}

}
