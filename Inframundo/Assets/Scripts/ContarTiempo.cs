﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContarTiempo : MonoBehaviour {

	public float maxTime = 30f;
	Text contador;
	GameObject interfaz;

	float currentTime = 0f;
	bool finished = false;
	bool started = false;

	// Use this for initialization
	void Awake () {
		interfaz = transform.Find ("ImagenContador").gameObject;
		interfaz.SetActive (false);
		contador = interfaz.GetComponentInChildren<Text> ();
		contador.text = maxTime.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		if (started) {
			if (!finished) {
				currentTime += Time.deltaTime;
				int valor = 30 - (int)Mathf.Round (currentTime);
				contador.text = valor.ToString ();
				finished = (valor <= 0);
			} else {
				interfaz.SetActive (false);
			}
		}
	}

	public bool isFinished() {
		return finished;
	}

	public void StartCounter() {
		started = true;
		interfaz.SetActive (true);
	}
}
