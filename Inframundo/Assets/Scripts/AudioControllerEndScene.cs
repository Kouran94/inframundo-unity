﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioControllerEndScene : MonoBehaviour {

	public AudioMixer masterMixer;
	// establecemos musica de fondo al nivel guardado
	void Start () {
		masterMixer.SetFloat("masterVol", PlayerPrefs.GetFloat ("masterVol"));
	}

}
