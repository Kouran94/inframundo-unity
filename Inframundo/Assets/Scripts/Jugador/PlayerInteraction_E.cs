﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInteraction_E : MonoBehaviour
{

    public GameObject bag;
    bool clicked;

    public float throwUp = 1000;
    public float throwPower = 5000;
    public float appearDistance = 2;
    GameObject AyudaInteraccion;

	void Start() {
		AyudaInteraccion = GameObject.Find ("Ayuda");
		AyudaInteraccion.transform.GetChild (0).gameObject.SetActive (false);
	}

    public GameObject ClosestTarget(string tag, float radio)
    {
        GameObject currentTarget = null;
        float distance = radio;

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag(tag))
        {
            //Vector3 diff = (obj.transform.position - transform.position);
            float curDistance = Vector3.Distance(transform.position, obj.transform.position);
            //Debug.Log(curDistance);
            if (curDistance < distance)
            {
                currentTarget = obj;
                distance = curDistance;
            }
        }
        return currentTarget;
    }

    private void Update()
    {
        if (bag) bag.GetComponent<Rigidbody>().transform.position = (this.transform.position + (transform.forward * appearDistance) +transform.up*2);
        if (Input.GetButtonDown("Interactuar"))
        {
            if (bag)
            {
                bag.GetComponent<Rigidbody>().AddForce((this.gameObject.transform.forward * throwPower) + (this.gameObject.transform.up * throwUp));
                bag.GetComponent<Rigidbody>().AddTorque(2, 4, 6);
                bag = null;
            }
            else
            {
                //Debug.Log("Interaction_E,Update, tecla e pulsada");

                GameObject throwable = ClosestTarget("Throwable", 2);

               	if (throwable)
                {
                    float distance = Vector3.Distance(transform.position, throwable.transform.position);
                    bag = throwable;
                }
                
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
		if (collision.gameObject.tag == "Throwable")
			AyudaInteraccion.transform.GetChild (0).gameObject.SetActive (true);
    }

    void OnCollisionExit(Collision collision)
    {
		AyudaInteraccion.transform.GetChild (0).gameObject.SetActive (false);
    }
}