﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDetector : MonoBehaviour {

	static Image greenFill; // Barra de sigilo del HUD
	static Image yellowFill; // Barra de sigilo del HUD

	// XXX: Estas constantes son redundantes porque dependen de datos definidos en otros sitios del juego pero ahora mismito no da tiempo a hacerlo bien...
	const float SUSPECT_TIME = 1f;
	const float COOLDOWN_TIME = 5f;

	static int detected = 0; // número de enemigos que te han detectado
	static int suspicious = 0; // número de enemigos que están a punto de detectarte
	static float minTimeForAlarm = float.PositiveInfinity; // tiempo que queda hasta que el enemigo al que menos le falta para detectarte lo haga
	static float maxTimeForCooldown = 0f; //  tiempo que queda hasta que al enemigo que más le falta para olvidarte lo haga

	bool alerted = false; // estamos persiguiendo?
	bool suspecting = false; // estamos sospechando?
	bool suspectedThisTick = false; // se ha llamado a suspect este turno?

	void Awake () {
		if(!greenFill) greenFill = GameObject.Find("StealthFillG").GetComponent<Image>();
		if(!yellowFill) yellowFill = GameObject.Find("StealthFillY").GetComponent<Image>();
	}

	// Llamamos a este método cuando el jugador ha sido definitivamente visto
	public void AlarmCooldown (float timeForCooldown) {
		if (!alerted) {
			//print (this.name + " started being alerted");
			alerted = true;
			detected += 1;
			//print ("There's " + detected + " alerted enemies");
		}

		if (maxTimeForCooldown < timeForCooldown) {
			maxTimeForCooldown = timeForCooldown;
			print (minTimeForAlarm);
		}
	}

	// Llamamos a este método cuando nos olvidamos de que el jugador está ahí
	public void AlarmEnd () {
		//print (this.name + " stopped being alerted");
		detected -= 1;
		alerted = false;
		//print ("There's " + detected + " alerted enemies");
	}

	// Llamamos a este método en cada update si el jugador está siendo visto, se pasa por parámetro el tiempo restante hasta la detección
	public void Suspect (float timeForAlarm) {
		
		if (!suspecting) {
			suspecting = true;
			suspicious += 1;
			//print (this.name + " started suspecting");
			//print ("There's " + suspicious + " suspecting enemies");
		}
		if (minTimeForAlarm > timeForAlarm) {
			minTimeForAlarm = timeForAlarm;
		}
		suspectedThisTick = true;
	}

	void LateUpdate () {
		UpdateOnce ();

		if (suspecting && !suspectedThisTick) {
			//print (this.name + " stopped suspecting");
			suspecting = false;
			suspicious -= 1;
			//print ("There's " + suspicious + " suspecting enemies");
		}
		suspectedThisTick = false;
	}

	// XXX: chapuza para tener una funcion que solo se llama una vez por tick independientemente del número de instancias
	static bool updatedOnce = false;
	static void UpdateOnce() {
		if (!updatedOnce) {
			if (suspicious > 0) {
				greenFill.fillAmount = minTimeForAlarm / SUSPECT_TIME;
				yellowFill.fillAmount = 1f;
			}

			if (detected > 0) {
				greenFill.fillAmount = 0f;
				yellowFill.fillAmount = 1 - (maxTimeForCooldown / COOLDOWN_TIME);
			}

			if (suspicious == 0 && detected == 0) {
				greenFill.fillAmount = 1f;
			}

			minTimeForAlarm = float.PositiveInfinity; // reseteamos el tiempo para la detección en cada late update
			maxTimeForCooldown = float.PositiveInfinity; // reseteamos el tiempo que falta para que se olviden
		}
		updatedOnce = true;
	}
	void Update() {
		if(updatedOnce) updatedOnce = false;
	}
}
