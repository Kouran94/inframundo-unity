﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*
* Script que engloba las mecánicas de vida del jugador.
*/
public class PlayerHealth : MonoBehaviour
{
	public float startingHealth = 100f;    // Salud inicial
	float currentHealth;                  // Salud actual

	float inmunity = 1f;				// Tiempo de inmunidad del jugaador
	float current_time_inmune = 0f; 	// Tiempo que el jugador ha estado inmune

	Animator anim; 
	PlayerMovement playerMovement;      // Script de movimiento del jugador
	Image fill;                  		// Barra de salud del HUD
	//	Color flashColour = new Color(.3f, .35f, .77f, .3f); // Color en el que parpadea el jugador herido
	//	float blinking_frequency = 0.1f;	//Frecuencia de parpadeo del jugador

	bool dead = false;                // Jugador vivo o muerto.
	bool damaged = false;                       // True when the player gets damaged.

	AudioSource audioSourc;
	public AudioClip[] dano;


	void Awake ()
	{
		playerMovement = GetComponent <PlayerMovement> ();
		anim = GetComponent <Animator> ();
		fill = GameObject.Find("HealthFill").GetComponent<Image>();
		audioSourc = GetComponent<AudioSource> ();
		// Set the initial health of the player.
		currentHealth = startingHealth;

		//StartCoroutine (Blink());
	}


	void Update ()
	{
		if (damaged)
			current_time_inmune += Time.deltaTime; 

		if (current_time_inmune >= inmunity)
			damaged = false;
	}

	public void TakeDamage (float amount)
	{
		// Set the damaged flag so the screen will flash.
		if (!damaged) {
			current_time_inmune = 0f;
			damaged = true;
			audioSourc.PlayOneShot (dano[(Mathf.RoundToInt(Random.value))%dano.Length],0.6F);
			// Reduce the current health by the damage amount.
			currentHealth -= amount;

			// Set the health bar's value to the current health.
			if (fill) {
				fill.fillAmount = currentHealth / startingHealth;
			}
			// If the player has lost all it's health and the death flag hasn't been set yet...
			if (currentHealth <= 0f && !dead)
				Death ();
		}

	}

	void Death ()
	{
		// Set the death flag so this function won't be called again.
		dead = true;
		anim.SetTrigger ("Die");

		// Detiene el movimiento del jugador cuando ha muerto.
		playerMovement.enabled = false;
	}   

	public void Heal(float amount) {
		//Si el jugador no está muerto, restaura cierta cantidad de vida
		if (!dead) {
			currentHealth += amount;
			currentHealth = currentHealth <= startingHealth ? currentHealth : startingHealth;
			if (fill) {
				fill.fillAmount = currentHealth / startingHealth;
			}
		}
	} 

	public bool isDead() {
		return dead;
	}

	public bool isDamaged() {
		return damaged;
	}
}