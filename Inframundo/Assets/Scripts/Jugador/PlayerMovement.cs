﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/*
*  Script de movimiento del jugador.
*  Maneja también las acciones de correr y ocultarse, 
*  así como las animaciones asociadas al movimiento.
*/
public class PlayerMovement : MonoBehaviour {

	public float normal_speed = 10f;            // Velocidad de movimiento normal.
	public float sprint_factor = 1.5f;			// Factor de aceleración cuando corremos
	public float hidden_speed = 3f;				// Velocidad cuando te mueves estando oculto
	Animator anim;         			 			// Componente de animación
	Light lantern;								// Luz de ocultación del jugador

	bool hidden = false;						// Bool de estado de ocultación
	bool shift = false;							// Bool de aceleración
	bool walk = false;							// Bool de movimiento

	float timer = 0f;							// Contador del tiempo que el jugador lleva oculto.

	//public AudioClip[] stepsSoundArray;

	AudioSource audioSourc;
	public AudioClip[] paso;
	public AudioClip agacharse;

	public float timeWaitWalk = 0.38F;
	public float timeWaitHide = 1.0F;
	public float timeWaitRun = 0.3F;	
	float countTime=0;
	bool oldHide;

	void Awake ()
	{
		anim = GetComponent <Animator> ();
		lantern = GetComponentInChildren<Light> ();
		audioSourc = GetComponent<AudioSource> ();
	}

	void FixedUpdate ()
	{
		// Valor de los ejes vertical y horizontal.
		float v = CrossPlatformInputManager.GetAxisRaw("Vertical");
		float h = CrossPlatformInputManager.GetAxisRaw("Horizontal");
		//Comprobamos si el jugador camina, corre o está oculto en función de las teclas pulsadas.

		hidden = Input.GetButton ("Hide");
		shift = Input.GetButton ("SprintForward");
		walk = !Mathf.Approximately (h, 0f) || !Mathf.Approximately (v, 0f);

		// Gira al jugador a la dirección en la que mira la cámara.
		//Turning ();

		// Mueve al jugador en las direcciones de los ejes y con las intensidades correspondientes
		Vector3 directionV = transform.forward;
		Vector3 directionH = transform.right;
		Vector3 direction = directionH * h + directionV * v;
		direction.Normalize ();

		if (isHidden() && !oldHide){
			Debug.Log("sonido de agacharse lanzado");
			audioSourc.PlayOneShot (agacharse);
		}
		oldHide = isHidden ();
		// Mueve al jugador hacia adelante si el valor de los ejes es distinto de cero.
		if (walk) {
			Move (direction, v, h);
			//
			if (isRunning () & (countTime > timeWaitRun)) {
				//GetComponent<AudioSource> ().PlayOneShot (stepsSoundArray [0]);
				audioSourc.PlayOneShot (paso[(Mathf.RoundToInt(Random.value))%paso.Length],0.6F);
				countTime = 0;
			} else if (isHidden () & (countTime > timeWaitHide)) {
				audioSourc.PlayOneShot (paso[(Mathf.RoundToInt(Random.value))%paso.Length],0.4F);
				//GetComponent<AudioSource> ().PlayOneShot (stepsSoundArray [0]);
				countTime = 0;
			} else if (countTime > timeWaitWalk & !isRunning() & !isHidden()) {
				audioSourc.PlayOneShot (paso[(Mathf.RoundToInt(Random.value))%paso.Length],0.5F);
				//GetComponent<AudioSource> ().PlayOneShot (stepsSoundArray [1]);
				countTime = 0;
			}
		}

		// Seleccionamos la animación a ejecutar en función del estado del jugador.
		Animating ();

		countTime += Time.deltaTime; 
	}


	void Move (Vector3 direction, float v, float h)
	{
		Vector3 dir = direction;

		// Asignamos la velocidad de movimiento en función del estado.
		float speed = hidden ? hidden_speed : normal_speed;

		// Si se ha pulsado la tecla de correr y no está oculto, aumentamos la velocidad.
		if (shift && !hidden)
			speed *= sprint_factor;

		// Trasladamos al jugador si está avanzando.
		if (!Mathf.Approximately(v,0))
			transform.Translate(dir * speed * Time.deltaTime, Space.World);

		// Rotamos al jugador salvo que se mueva hacia atrás.
		if (!(Mathf.Approximately(h,0) && Mathf.Approximately(v,-1)))
			transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (direction), 0.05f);

	}

	void Turning ()
	{
		transform.forward = Vector3.Scale(Camera.main.transform.forward, Vector3.forward + Vector3.right);
	}


	void Animating ()
	{
		anim.SetBool ("Move", walk);
		anim.SetBool ("Run", shift && walk);
		//si el jugador está oculto, aumentamos el contador de tiempo oculto
		timer = hidden ? timer+Time.deltaTime : 0f;
		lantern.enabled = !hidden;
		anim.SetBool("Hidden", hidden);
	}

	public bool isHidden() {
		return hidden;
	}

	public bool isWalking() {
		return walk;
	}

	public bool isRunning() {
		return shift;
	}

	public float getHiddenTime() {
		return timer;
	}


}
