using UnityEngine;
using System.Collections;

public class Hit : MonoBehaviour
{

	public GameObject DestroyedObject;
    public float timeUnBreakable = 5f;
    public bool breakable = false;
	AudioSource sonido;

    private void Start()
    {
		sonido = GetComponent<AudioSource> ();
		if (sonido != null)
			sonido.enabled = false;
        Invoke("isBreakable", timeUnBreakable);
    }

    void OnCollisionEnter (Collision collision)
	{
		if (collision.relativeVelocity.magnitude > 5f && breakable) {
			DestroyIt ();
		}
	}

	void DestroyIt ()
	{
		if (DestroyedObject) {
			Instantiate (DestroyedObject, transform.position, transform.rotation);
		}
		Destroy (gameObject);
	}

	void isBreakable() {
		breakable = true;
		if (sonido != null)
			sonido.enabled = true;
	}
}