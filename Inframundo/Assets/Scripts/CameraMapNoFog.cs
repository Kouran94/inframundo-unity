﻿using UnityEngine;

/* 
 * Script para que una cámara ignore la niebla
 * Lo utiliza la cámara del mapa. 
*/
public class CameraMapNoFog : MonoBehaviour {

	bool doWeHaveFogInScene;

	private void Start() {
		doWeHaveFogInScene = RenderSettings.fog;
	}

	private void OnPreRender() {
		RenderSettings.fog = false;
	}
	private void OnPostRender() {
		RenderSettings.fog = doWeHaveFogInScene;
	}
}