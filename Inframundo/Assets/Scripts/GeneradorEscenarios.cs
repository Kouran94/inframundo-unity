﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 * Script de generación del mapa de habitaciones aleatorio para cada nivel, 
 * incluyendo tanto las paredes como las uniones entre habitaciones.
 */
public class GeneradorEscenarios : MonoBehaviour
{
	/** Interfaz externa del script **/
	public int GRID_MAX_Y;
	// numero de filas
	public int GRID_MAX_X;
	// numero de columnas
	public int ROOM_WIDTH;
	public int ROOM_HEIGHT;
	public int rooms;
	// numero de habitaciones
	public GameObject muro;
	public GameObject muro_esquina;
	public GameObject muro_fin;
	public GameObject hab_collider;

	/** Constantes **/
	const int TILE_SIDE = 5;
	Quaternion V_ROTATION = Quaternion.Euler (0, 90, 0);
	// cuaternión para la posición vertical (rotar 90º sobre el eje y)

	/** Variables **/
	VertexNode[,] map = null;

	private GameObject startRoom;

	private void CrearNivel ()
	{
		map = InicializarMapa ();
		ColocarMuros ();
	}

	/**
	 * Crea un muro a base de posicionar bloques de muro en la dirección especificada.
	 **/
	private void ColocarMuro (int startX, int startZ, bool horizontal, bool door, string name)
	{
		GameObject muralla = new GameObject ();
		muralla.name = "Muralla " + name;
		muralla.transform.parent = this.transform;
		muralla.transform.position = this.transform.TransformPoint (muralla.transform.position);
		int stop = horizontal ? ROOM_WIDTH - 1 : ROOM_HEIGHT - 1;
		for (int i = 1; i < stop; i++) {
			if (!door || i != stop / 2) { // si la muralla no tiene puerta o no estamos en el medio ponemos muro x)
				int x = horizontal ? i * TILE_SIDE + startX : startX;
				int z = horizontal ? startZ : i * TILE_SIDE + startZ;
				// instanciamos el muro en su sitio
				GameObject bloque;
				if (i != (stop / 2 - 1) || i != (stop / 2 + 1)) {
					
					bloque = (GameObject)Instantiate (muro, muralla.transform.TransformPoint(new Vector3 (x, 0, z)), horizontal ? Quaternion.identity : V_ROTATION);
					bloque.transform.parent = muralla.transform;
				}
			}
		}
	}

	private void ColocarHabitacion(float x, float z, string name, bool inicial) {
		GameObject habitacion = (GameObject)Instantiate (hab_collider, this.transform.TransformPoint (new Vector3(x, 0, z)), Quaternion.identity);
		habitacion.name = name;
		habitacion.transform.parent = this.transform;
		habitacion.transform.localScale = new Vector3 (TILE_SIDE*(ROOM_WIDTH-2), TILE_SIDE, TILE_SIDE*(ROOM_HEIGHT-1));
		if (inicial) {
			habitacion.tag = "Start";
			startRoom = habitacion;
		} else {
			habitacion.tag = "Rooms";
		}
	}

	private void ColocarEsquina (int x, int z, string name)
	{
		GameObject esquina = (GameObject)Instantiate (muro_esquina, new Vector3 (x, 0, z), Quaternion.identity);
		esquina.name = name;
		esquina.transform.parent = this.transform;
		esquina.transform.position = this.transform.TransformPoint (esquina.transform.position);
	}

	private bool ExisteHabitacion (int j, int i)
	{
		return (i < GRID_MAX_Y && i >= 0) && (j < GRID_MAX_X && j >= 0) && map [i, j].active;
	}

	/**
	 * Crea los muros del nivel.
	 **/
	private void ColocarMuros ()
	{
		// NORTE Y ESTE SON POSITIVOS EN UNITY (las dimensiones crecen como en un eje cartesiano de toda la vida)
		int displaceX = (ROOM_WIDTH - 1) * TILE_SIDE;
		int displaceZ = (ROOM_HEIGHT - 1) * TILE_SIDE;
		for (int i = 0; i < GRID_MAX_Y; i++) {
			for (int j = 0; j < GRID_MAX_X; j++) {
				if (map [i, j].active) {
					ColocarHabitacion (j*displaceX+displaceX*0.5f, i*displaceZ+displaceZ*0.5f, "Habitacion " + j + " " + i, map[i,j].inicial);

					VertexNode v = map [i, j];
					// colocar muro sur
					ColocarMuro (j * displaceX, i * displaceZ, true, (v.s != null && v.s.open), j + " " + i + " S");
					ColocarEsquina (j * displaceX, i * displaceZ, "Esquina " + j + " " + i + " SW");

					if (!ExisteHabitacion (j, i + 1)) {
						// colocar muro norte (solo si no hay habitación al norte, si no se colocará como muro sur de esa habitación)
						ColocarMuro (j * displaceX, (i + 1) * displaceZ, true, false, j + " " + i + " N");
						ColocarEsquina (j * displaceX, (i + 1) * displaceZ, "Esquina " + j + " " + i + " NW");
					}

					// colocar muro oeste
					ColocarMuro (j * displaceX, i * displaceZ, false, (v.w != null && v.w.open), j + " " + i + " W");

					if (!ExisteHabitacion (j + 1, i)) {
						// colocar muro este (solo si no hay habitación al este, si no se colocará como muro oeste de esa habitación)
						ColocarMuro ((j + 1) * displaceX, i * displaceZ, false, false, j + " " + i + " E");
						// La esquina sudeste se coloca solo si además no hay habitación al sudeste, 
						if(!ExisteHabitacion(j+1,i-1)) {
							ColocarEsquina ((j + 1) * displaceX, i * displaceZ, "Esquina " + j + " " + i + " SE");
						}

						// La esquina nordeste se coloca solo si además no hay habitación al norte ni al sudeste (dios mío...)
						if (!ExisteHabitacion (j, i + 1) && !ExisteHabitacion (j + 1, i + 1)) {
							ColocarEsquina ((j + 1) * displaceX, (i + 1) * displaceZ, "Esquina " + j + " " + i + " NE");
						}
					}
				}
			}
		}	
	}

	/**
	 * Clase que representa vértices en el grafo de habitaciones
	 **/
	private class VertexNode : System.IEquatable<VertexNode>, System.IComparable<VertexNode>
	{
		public const short N = 0, S = 1, E = 2, W = 3;
		public int x, y;
		public EdgeNode n, s, e, w;
		public bool active = false;
		public bool inicial = false;

		public bool open {
			get {
				// todas las aristas que salen de este vértice o bien son nulas o bien están abiertas
				return (n == null || n.open) && (s == null || s.open) && (e == null || e.open) && (w == null || w.open);
			}
		}

		public bool Equals (VertexNode other)
		{
			return other != null && this.x == other.x && this.y == other.y;
		}

		public int CompareTo (VertexNode other)
		{
			return this.MinVertex ().CompareTo (other.MinVertex ());
		}

		/**
		 * Weight of the minimum-weight open edge for this room (or Infinity if no edge is open)
		 **/
		public float MinVertex ()
		{
			float min = float.PositiveInfinity;
			if (n != null && !n.open && n.weight < min)
				min = n.weight;
			if (s != null && !s.open && s.weight < min)
				min = s.weight;
			if (e != null && !e.open && e.weight < min)
				min = e.weight;
			if (w != null && !w.open && w.weight < min)
				min = w.weight;
			
			return min;
		}

		public override string ToString ()
		{
			string exits = (n != null ? "n" : "") + (s != null ? "s" : "") + (e != null ? "e" : "") + (w != null ? "w" : "");
			return string.Format ("[VertexNode: (x,y)=({0},{1}), open={2}, active={3}, edges={4}]", x, y, open, active, exits);
		}
	}

	/**
	 * Clase que representa aristas (puertas o direcciones) en el grafo de habitaciones
	 **/
	private class EdgeNode : System.IComparable<EdgeNode>
	{
		public float weight;
		public bool open;

		public int CompareTo (EdgeNode other)
		{
			return weight.CompareTo (other);
		}
	}

	/**
	 * La carga pesada del algoritmo está aquí. Inicializa un grafo de habitaciones aleatorio
	 * y luego lo recorre por mínimo pesos creando habitaciones y puertas como sea necesario.
	 * El concepto es similar al de Inframundo 1 pero ahora una habitación no está necesariamente
	 * conectada a todas las circundantes, dándole un feel más de laberinto. ;^)
	 **/
	private VertexNode[,] InicializarMapa ()
	{
		VertexNode[,] pesos = new VertexNode[GRID_MAX_Y, GRID_MAX_X];

		// inicializamos grafo con pesos aleatorios
		for (int i = 0; i < GRID_MAX_Y; i++) {
			for (int j = 0; j < GRID_MAX_X; j++) {
				pesos [i, j] = new VertexNode ();
				pesos [i, j].x = j;
				pesos [i, j].y = i;
				EdgeNode edge = null;
				if (i > 0) {
					pesos [i, j].s = pesos [i - 1, j].n;
				}
				if (i < GRID_MAX_Y - 1) {
					edge = new EdgeNode ();
					edge.weight = Random.value;
					edge.open = false;
					pesos [i, j].n = edge;
				}
				if (j > 0) {
					pesos [i, j].w = pesos [i, j - 1].e;
				}
				if (j < GRID_MAX_X - 1) {
					edge = new EdgeNode ();
					edge.weight = Random.value;
					edge.open = false;
					pesos [i, j].e = edge;
				}
			}
		}

		// recorrido de mínimo peso
		VertexNode inicial = pesos [(int)(Random.value * GRID_MAX_Y), (int)(Random.value * GRID_MAX_X)];
		VertexNode v = inicial;
		//VertexNode inicial = pesos [0,0];
		List<VertexNode> frontera = new List<VertexNode> ();
		List<VertexNode> visitadas = new List<VertexNode> ();
		frontera.Add (inicial);
		visitadas.Add (inicial);
		inicial.active = true;
		inicial.inicial = true;
		while (visitadas.Count < rooms) {

			// obtenemos la arista mínima
			if (!v.open) {
				float min = v.MinVertex ();
				// elegimos la arista mínima, la marcamos como visitada
				// y añadimos a la frontera el nuevo vértice
				VertexNode discovered = null;
				if (v.s != null && !v.s.open && Mathf.Approximately (v.s.weight, min)) {
					v.s.open = true;
					discovered = pesos [v.y - 1, v.x];
				} else if (v.n != null && !v.n.open && Mathf.Approximately (v.n.weight, min)) {
					v.n.open = true;
					discovered = pesos [v.y + 1, v.x];
				} else if (v.e != null && !v.e.open && Mathf.Approximately (v.e.weight, min)) {
					v.e.open = true;
					discovered = pesos [v.y, v.x + 1];
				} else if (v.w != null && !v.w.open && Mathf.Approximately (v.w.weight, min)) {
					v.w.open = true;
					discovered = pesos [v.y, v.x - 1];
				}

				if (!visitadas.Contains (discovered)) {
					discovered.active = true;
					visitadas.Add (discovered);
				}

				if (!frontera.Contains (discovered)) {
					frontera.Add (discovered);
				}
			} else {
				// si todas las aristas posibles están ya abiertas
				// eliminamos de la frontera
				frontera.RemoveAt (0);
			}

			frontera.Sort (); // ordenamos la frontera por peso mínimo (considerando solo los vértices que no estén todavía abiertos)
			v = frontera [0];
		}
		return pesos;
	}

	void Start ()
	{
		CrearNivel ();
		gameObject.GetComponent<GeneradorContenido> ().GenerarContenidos (this.startRoom);
	}
}
