﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Distraction : MonoBehaviour {

	/** Tiempo que la distracción está activa **/
	public float timeToLive;
	public bool onStart;

	void Start () {
		CreateDistraction ();
	}

	public void CreateDistraction () {
		if(!Distractible.AddDistraction (this.transform.position, timeToLive))
			Debug.Log ("Descartando distracción, cola llena.");
	}
}
