﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveRBWhenStable : MonoBehaviour {

	public float timeToStable;
	Vector3 lastVelocity;
	float timeStable;

	void OnEnabled () {
		if (GetComponent<Rigidbody> ()) {
			lastVelocity = GetComponent<Rigidbody> ().velocity;
		} else {
			enabled = false;
			print ("El objeto no tiene un RigidBody que eliminar.");
		}
	}

	// Update is called once per frame
	void Update () {
		if (IsStable ()) {
			timeStable += Time.deltaTime;
			if (timeStable >= timeToStable) {
				Destroy (GetComponent<Rigidbody> ());
				enabled = false;
			}
		} else {
			timeStable = 0;
		}
	}

	bool IsStable() {
		float acceleration = (lastVelocity - GetComponent<Rigidbody> ().velocity).magnitude / Time.deltaTime;
		return Mathf.Approximately (acceleration, 0);
	}
}
