﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    public int damage;
    public float speed;

    public void Shoot(Vector3 direction)
    {
        Vector3 velocity = direction  * speed;
        //Debug.Log("Shoot. X: "+velocity.x+", Y:"+velocity.y+", Z:"+velocity.z);
        GetComponent<Rigidbody>().velocity = velocity;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //Debug.Log("Bullet damage Player by "+damage);
            other.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
        }
        if (!other.isTrigger)
        {
            //Debug.Log("Bullet destroyed by " + other.gameObject);
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        //Debug.Log("Bullet destroyed");
        Destroy(gameObject);
    }
}