﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbsTrap : MonoBehaviour {

    //Número de veces que se activa la trampa. < 0 : infinitas
    public int uses;

    public void OnTriggerEnter(Collider other)
    {
        if (uses != 0 && other.gameObject.tag == "Player")
        {
            executeTrap();
            if(uses > 0)
                uses--;
        }

    }

    public abstract void executeTrap();
}
