﻿using UnityEngine;

public class TrapShoot : AbsTrap {

    public GameObject bullet;
    private GameObject player;

    // Use this for initialization
    void Start () {
        player = GameObject.Find("Player");
	}

    public override void executeTrap()
    {
        if(bullet != null)
        {
            Vector3  size = gameObject.GetComponent<Collider>().bounds.size;
            Vector3 pos = new Vector3(0,0,0);
            int r = Random.Range(0,2);
            switch(r)
            {
                case 0:
                    pos.x = size.x * 3;
                    break;
                case 1:
                    pos.y = size.y * 3;
                    break;
                case 2:
                    pos.z = size.z * 3;
                    break;
            }
            GameObject localBullet = 
                Instantiate(bullet, pos, bullet.transform.rotation, gameObject.transform);
            localBullet.GetComponent<BulletScript>().speed = 2;
            localBullet.GetComponent<BulletScript>().Shoot(player.transform.position - localBullet.transform.position);
        }
    }
}
