﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ActivatePortal : MonoBehaviour {

	Light luz;
	ParticleSystem portal;

	bool active = false;

	// Use this for initialization
	void Start () {
		luz = gameObject.GetComponentInChildren<Light> ();
		portal = gameObject.GetComponentInChildren<ParticleSystem> ();
		luz.enabled = false;
		portal.Stop ();
	}
	
	// Update is called once per frame
	public void Activate() {
		if (!active) {
			active = true;
			luz.enabled = true;
			portal.Play ();
		}
	}

	//Cuando el jugador está dentro del collider del portal, si pulsa el botón de interactuar 
	// se comprueba si se cumplen los requisitos para pasar de nivel.
	void OnTriggerStay(Collider col) {
		if (active && col.transform.tag == "Player" && Input.GetButtonDown("Interactuar")) {
			int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
			SceneManager.LoadScene(nextSceneIndex);
		}

	}
}
