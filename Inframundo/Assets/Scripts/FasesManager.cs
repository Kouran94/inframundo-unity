﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FasesManager : MonoBehaviour {

	//Bosses' battle elements
	GameObject altarAzul;
	GameObject altarRojo;
	GameObject altarVerde;

	List<Toggle> toggles;
	Text done;

	//Bosses
	EacusAttack eaco;
	MinosAttack minos;
	RadamantisAttack radamantis;

	ActivatePortal portal;

	GameObject hud;

	bool onBattle = false;
	bool used = false;
	bool minosDefeated = false;
	bool eacoDefeated = false;
	bool radamantisDefeated = false;

	// Use this for initialization
	void Start () {
		hud = GameObject.Find ("HUDFinal");
		hud.transform.GetChild (3).gameObject.SetActive (true);
		hud.transform.GetChild (4).gameObject.SetActive (false);
		hud.transform.GetChild (5).gameObject.SetActive (false);
		altarRojo = transform.FindChild("RedAltar").gameObject;
		altarAzul = transform.FindChild("BlueAltar").gameObject;
		altarVerde = transform.FindChild("GreenAltar").gameObject;
		eaco = altarAzul.transform.FindChild("Eaco").gameObject.GetComponent<EacusAttack>();
		minos = altarRojo.transform.FindChild("Minos").gameObject.GetComponent<MinosAttack> ();
		radamantis = altarVerde.transform.FindChild("Radamantis").gameObject.GetComponent<RadamantisAttack>();
		portal = GameObject.Find ("Portal").GetComponent<ActivatePortal> ();
		GetToggles(GameObject.FindGameObjectsWithTag("Toggle"));
		done = toggles [0].gameObject.transform.parent.GetComponentsInChildren<Text> () [0];
	}
	
	// Update is called once per frame
	void Update () {
		if (!used && minosDefeated && eacoDefeated && radamantisDefeated) {
			//unlock portal and stairs
			used = true;
			done.enabled = true;
			portal.Activate ();
		}
	}

	public void parseActivation(GameObject altar) { 
		if (!onBattle && !used) {
			onBattle = true;
			altar.transform.FindChild ("Batalla").gameObject.SetActive (true);
			altar.transform.FindChild ("Barrier").gameObject.SetActive (false);
			if (altar.name.Contains ("Red")) {
				hud.transform.GetChild (4).gameObject.SetActive (true);
				minos.enabled = true;
			} else if (altar.name.Contains ("Blue")) {
				eaco.enabled = true;
			} else if (altar.name.Contains ("Green")) {
				hud.transform.GetChild (5).gameObject.SetActive (true);
				radamantis.enabled = true;
			}
		}
	}

	public void parseDeactivation(GameObject altar) {
		if (onBattle) {
			onBattle = false;
			altar.transform.FindChild ("Batalla").gameObject.SetActive (false);
			if (altar.name.Contains ("Red")) {
				unactivateRed ();
				hud.transform.GetChild (4).gameObject.SetActive (false);
			} else if (altar.name.Contains ("Blue")) {
				unactivateBlue ();
			} else if (altar.name.Contains ("Green")) {
				unactivateGreen ();
				hud.transform.GetChild (5).gameObject.SetActive (true);
			}
		}
	}

	public bool isOnBattle() {
		return onBattle;
	}

	void unactivateRed() {
		checkToggle (1);
		minos.enabled = false;
		minos.gameObject.SetActive(false);
		minosDefeated = true;
	}

	void unactivateBlue() {
		checkToggle (0);
		eaco.enabled = false;
		eaco.gameObject.SetActive(false);
		eacoDefeated = true;
	}

	void unactivateGreen() {
		checkToggle (2);
		radamantis.enabled = false;
		radamantis.gameObject.SetActive(false);
		radamantisDefeated = true;
	}

	void GetToggles(GameObject[] objects) {
		toggles = new List<Toggle> ();
		foreach (GameObject obj in objects) {
			toggles.Add (obj.GetComponent<Toggle> ());
		}
	}

	void checkToggle(int i) {
		toggles [i].isOn = true;
		ColorBlock color = toggles [i].colors;
		color.colorMultiplier = 5;
	}
}
