﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Script que añade una fueraza vertical a la bola del interruptor, para hacerla flotar. */
public class InterruptorAddForce : MonoBehaviour {

	float hoverForce = 20f;
	bool active = false;

	void OnEnable() {
		active = true;
	}
		
	void OnTriggerStay(Collider col) {
		if (active && col.gameObject.name == "HoveringBall") {
			col.attachedRigidbody.AddForce (Vector3.up * hoverForce, ForceMode.Acceleration);
			col.attachedRigidbody.AddTorque (Vector3.up * hoverForce, ForceMode.Acceleration);
		}
	}
}
