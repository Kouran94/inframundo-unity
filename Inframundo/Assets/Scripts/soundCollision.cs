﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundCollision : MonoBehaviour {

    public float timeMute = 4;
    AudioSource sonido;

	// Use this for initialization
	void Start () {
        sonido = GetComponent<AudioSource>();
        if (sonido != null)
            sonido.enabled = false;
        Invoke("unMute", timeMute);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > 5f)
        {
            sonido.Play();
        }
    }

    void unMute()
    {
        sonido.enabled=true;
    }
}
